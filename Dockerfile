FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN apt-get update && apt-get install -y openscad

WORKDIR /app/code
RUN git clone https://github.com/seasick/openscad-web-gui.git .
RUN npm install

EXPOSE 3000 8000 8888

COPY start.sh /app/code/start.sh
RUN chmod +x /app/code/start.sh

VOLUME /app/data

CMD ["/app/code/start.sh"]
