#!/bin/bash

# Define and verify necessary directories
export LOG_PATH="/app/data/logs"
mkdir -p "$LOG_PATH"

echo "Starting the OpenSCAD Web GUI ..."
# Run the production serving script
npm run serve >> "$LOG_PATH/openscad-web-gui.log" 2>&1
